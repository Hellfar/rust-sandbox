use std::env;
use std::fmt;

#[derive(PartialEq)]
#[derive(Debug)]
enum TokenType {
  Number,
  Operator,
  Grouping(String),
  DeGrouping(String),
  None
}

#[derive(Debug)]
struct TokenSlice {
  start: u128,
  offset: u128
}

#[derive(Debug)]
struct Token {
  slice: TokenSlice,
  typ: TokenType,
}

// fn letter_is_present(s: String, c: char) {
//   s.chars().position(|e| e == c).unwrap();
// }

fn lexer(query: &String) {
  let mut cur_tok: Token = Token {
    slice: TokenSlice {
      start: 0,
      offset: 0
    },
    typ: TokenType::None
  };
  let mut tokenized: Vec<Token> = vec![];

  for (i, c) in query.chars().enumerate() {
    #[cfg(debug_assertions)]
    println!("{} {}", i, c);
    let tokenType: &TokenType;

    if c.is_digit(10) {
      tokenType = &TokenType::Number;

      // let token: Token = Token {
      //   c: c.to_string(),
      //   t: TokenType::Number
      // };
      // println!("{:?}", token);
    }
    else {
      tokenType = &TokenType::None;
    }

    if cur_tok.typ == TokenType::None {
      cur_tok.typ = *tokenType;
    }
    if cur_tok.typ != *tokenType {
      println!("{:?}", tokenType);
      tokenized.push(cur_tok);
      cur_tok = Token {
        slice: TokenSlice {
          start: 0,
          offset: 0
        },
        typ: TokenType::None
      };
    }
  }
}

fn main() {
  let args: Vec<String> = env::args().collect();

  #[cfg(debug_assertions)]
  println!("{:?}", args);

  if args.len() > 1 {
    lexer(&args[1]);
  }

  let ys = [50; 5];
  println!("{:?}", ys);
}
